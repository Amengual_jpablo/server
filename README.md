

## Getting Started

Primero, clonamos el proyecto y vamos a la carpeta ~/server y ejecutamos
```bash
npm install
```

Cuando se terminen de instalar las dependencias presionamos
```bash
npm start
```

La aplicación estará ejecutandose en [http://localhost:4000](http://localhost:4000) 