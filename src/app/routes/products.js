
const mysql = require('mysql');

module.exports = app => {

    app.get('/', (req, res) => {
        var connection = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USER,
            password: process.env.PASSWORD,
            database: process.env.DATABASE
        });
        connection.connect();
        connection.query('SELECT * FROM product', (err, result) => {
            if (result !== undefined) {
                res.send(result);
            } else {
                res.send(err);
            }
        });
        connection.end();
    });
}