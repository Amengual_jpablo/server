
const app = require('./config/server');

require('./app/routes/products')(app);

app.listen(app.get('port'), () =>{
    console.log("Aplicación ejecutandose en localhost:4000");
});